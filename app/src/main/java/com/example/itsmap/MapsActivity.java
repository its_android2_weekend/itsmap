package com.example.itsmap;

import androidx.fragment.app.FragmentActivity;

import android.location.Address;
import android.location.Geocoder;
import android.os.Bundle;
import android.util.Log;
import android.widget.Toast;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

public class MapsActivity extends FragmentActivity implements OnMapReadyCallback {

    private static final String TAG = "123456";
    private GoogleMap mMap;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_maps);
        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);
    }


    /**
     * Manipulates the map once available.
     * This callback is triggered when the map is ready to be used.
     * This is where we can add markers or lines, add listeners or move the camera. In this case,
     * we just add a marker near Sydney, Australia.
     * If Google Play services is not installed on the device, the user will be prompted to install
     * it inside the SupportMapFragment. This method will only be triggered once the user has
     * installed Google Play services and returned to the app.
     */
    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;

        LatLng royalPalace = new LatLng(11.564428, 104.931090);
        mMap.addMarker(new MarkerOptions().position(royalPalace).title("Royal Palace"));
        mMap.setMapType(GoogleMap.MAP_TYPE_NORMAL);
        mMap.setTrafficEnabled(true);
        mMap.getUiSettings().setCompassEnabled(true);
        mMap.getUiSettings().setZoomControlsEnabled(true);
        mMap.getUiSettings().setMapToolbarEnabled(true);
        mMap.getUiSettings().setIndoorLevelPickerEnabled(true);
        mMap.moveCamera(CameraUpdateFactory.newLatLng(royalPalace));
        CameraPosition cameraPosition = new CameraPosition.Builder().target(royalPalace).zoom(15).build();
        mMap.animateCamera(CameraUpdateFactory.newCameraPosition(cameraPosition));

        mMap.setOnMapClickListener(new GoogleMap.OnMapClickListener() {
            @Override
            public void onMapClick(LatLng latLng) {
                Log.d(TAG,"You click at LatLng [" + latLng.latitude + ", "+ latLng.longitude +"]");
                Address address = getLocationName(latLng);
                List<LatLng> latLngs = reverseGeoCoding(address.getAddressLine(0));
                if(address != null){

                    Toast.makeText(
                            MapsActivity.this,
                            "Location LatLng { "
                                    + latLngs.get(0).latitude
                                    +" , "
                                    + latLngs.get(0).longitude,

                            Toast.LENGTH_SHORT).show();

                    MarkerOptions maker  = new MarkerOptions()
                            .position(latLng)
                            .title(address.getAddressLine(0))
                            .snippet(address.getCountryName());
                    maker.icon(BitmapDescriptorFactory.fromResource(R.mipmap.ic_marker_custom));
                    mMap.addMarker(maker).showInfoWindow();
                }
            }
        });
    }
    private List<LatLng>  reverseGeoCoding ( String strLocationName){
        List<LatLng> ll = new ArrayList(); // A list to save the coordinates if they are available
        if(Geocoder.isPresent()){
            try {
                Geocoder gc = new Geocoder(this);
                List<Address> addresses= gc.getFromLocationName(strLocationName, 1); // get the found Address Objects

                for(Address a : addresses){
                    if(a.hasLatitude() && a.hasLongitude()){
                        ll.add(new LatLng(a.getLatitude(), a.getLongitude()));
                    }
                }
            } catch (IOException e) {
                // handle the exception
            }
        }
        return ll;
    }
    private Address getLocationName(LatLng latLng) {
        Geocoder geocoder = new Geocoder(getBaseContext(), Locale.getDefault());
        try {
            List<Address> addresses = geocoder.getFromLocation(latLng.latitude,latLng.longitude,1);
            if(addresses != null & addresses.size()>0){
                return addresses.get(0);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }
}
